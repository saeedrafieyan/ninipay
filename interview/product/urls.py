from django.urls import path, include
from rest_framework import routers
from product import views


router = routers.DefaultRouter()
router.register('category', views.CategoryViewSet)
router.register('subcategory', views.SubCategoryViewSet)
router.register('brand', views.BrandViewSet)
router.register('product', views.ProductViewSet)
router.register('comment', views.CommentViewSet)


urlpatterns = [
    path('', include(router.urls)),
    # path('subcategory/',views.SubCategoryViewSet.as_view({'get':'retrieve'}))
]