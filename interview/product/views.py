from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets
from .serializer import *
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return DetailCategorySerializer
        return ListCategorySerializer


class SubCategoryViewSet(viewsets.ModelViewSet):
    queryset = SubCategory.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return DetailSubCategorySerializer
        return DetailSubCategorySerializer


class BrandViewSet(viewsets.ModelViewSet):
    queryset = Brand.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return DetailBrandSerializer
        return ListBrandSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()

    def get_queryset(self):
        queryset = Product.objects.all()
        request = self.request.query_params
        price = request.get('price', None)
        quantity = request.get('quantity', None)
        if price is not None:
            queryset = Product.objects.filter(price=price)
        if quantity:
            queryset = Product.objects.filter(quantity=quantity)

        return queryset

    @csrf_exempt
    def get_serializer_class(self):
        if self.action == 'retrieve':
            return DetailProductSerializer
        return DetailProductSerializer


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    # permission_classes = [IsAuthenticated, ]
    # authentication_classes = [TokenAuthentication, ]

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return DetailCommentSerializer
        elif self.action == 'create':
            return CreateCommentSerializer
        return ListCommentSerializer
