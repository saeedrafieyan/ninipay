from django.contrib.auth.models import User
from django.db import models


class BaseImage(models.Model):
    """
    all of the images in the system are here and we can add multiple picture to a specific product
    """
    name = models.CharField(max_length=100, null=False, blank=False)
    image = models.ImageField(upload_to='interview/static/images', null=True, blank=True)

    def __str__(self):
        return self.name


class Category(models.Model):
    """
    in this section we should define main categories like clothing, washing, food and etc.
    """
    name = models.CharField(max_length=100, null=False, blank=False, unique=True)
    image = models.ForeignKey(BaseImage, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    """
    In this section we have to specify what is the subcategories of each category. For example, baby clothes, hat, socks
     and etc. fall into the clothing category.
    """
    name = models.CharField(max_length=100, null=False, blank=False, unique=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='sub', null=False, blank=False)

    def __str__(self):
        return self.name


class Brand(models.Model):
    """
    all brands has a foreignkey to the SubCategory and if a subcategory is deleted, the brands in it will remain.
    """
    name = models.CharField(max_length=100, null=False, blank=False, unique=True)
    SubCategory = models.ForeignKey(SubCategory, related_name='brand', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Product(models.Model):
    """
    each product has a certain attribute like brand,price,detail and others.
    """
    name = models.CharField(max_length=100, null=False, blank=False)
    brand = models.ForeignKey(Brand, related_name='product', on_delete=models.CASCADE)
    price = models.IntegerField()
    detail = models.TextField(max_length=1000)
    quantity = models.IntegerField()
    add_time = models.DateField(auto_now_add=True, null=True, blank=True)
    product_images = models.ManyToManyField(BaseImage, blank=True)

    class Meta:
        ordering = ['add_time']

    def __str__(self):
        return self.name


class Comment(models.Model):
    """
    in the comment section users can say their opinion to a product and take a rate to it
    """
    rate_choices = [
        ('1', '*'),
        ('2', '**'),
        ('3', '***'),
        ('4', '****'),
        ('5', '*****'),
    ]
    author = models.ForeignKey(User, on_delete=models.CASCADE,related_name='comment_user', null=True)
    product = models.ForeignKey(Product, related_name='comment', on_delete=models.CASCADE)
    comment = models.CharField(max_length=500)
    rate = models.CharField(max_length=5, choices=rate_choices, blank=True, null=True)
    is_publish = models.BooleanField(default=False)

    def __str__(self):
        return self.author
