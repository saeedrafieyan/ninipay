from rest_framework import serializers
from .models import Product, Brand, Category, SubCategory, Comment


class ListCommentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Comment
        fields = ['product', 'rate', 'url']


class CreateCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ['comment', 'product', 'rate']


class DetailCommentSerializer(serializers.HyperlinkedModelSerializer):
    product = ListCommentSerializer(many=True, read_only=True)

    class Meta:
        model = Comment
        fields = ['comment', 'product', 'rate', 'url']


class DetailProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = ['name', 'brand', 'price', 'detail', 'quantity']


class ListProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = ['name', 'brand', 'price']


class DetailBrandSerializer(serializers.HyperlinkedModelSerializer):
    product = ListProductSerializer(many=True, read_only=True)

    class Meta:
        model = SubCategory
        fields = ['name', 'product', 'url']


class ListBrandSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Brand
        fields = ['name', 'SubCategory', 'url']


class DetailSubCategorySerializer(serializers.HyperlinkedModelSerializer):
    brand = ListBrandSerializer(many=True, read_only=True)

    class Meta:
        model = SubCategory
        fields = ['name', 'brand','category' , 'url']


class ListSubCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SubCategory
        fields = ['name', 'url']


class DetailCategorySerializer(serializers.ModelSerializer):
    sub = ListSubCategorySerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ['name', 'sub', 'image']


class ListCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = ['name', 'url', 'image']


