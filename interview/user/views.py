from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from .serializer import *


class ProfileViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    authentication_classes = [JSONWebTokenAuthentication]
    queryset = Profile.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return DetailProfileSerializer
        return ListProfileSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return DetailUserSerializer
        return DetailUserSerializer


class ChildrenViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Children.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return DetailChildrenSerializer
        return ListChildrenSerializer


class shippingViewSet(viewsets.ModelViewSet):
    queryset = ShippingAddress.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return DetailShippingAddressSerializer
        return ListShippingAddressSerializer


