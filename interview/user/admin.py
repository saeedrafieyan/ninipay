from django.contrib import admin
from .models import Profile, Children, ShippingAddress


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'first_name', 'last_name', 'gender']
    list_filter = ['gender', ]


@admin.register(Children)
class ChildrenAdmin(admin.ModelAdmin):
    list_display = ['profile', 'child_name', 'age', 'gender']
    list_filter = ['gender']


@admin.register(ShippingAddress)
class ShippingAdmin(admin.ModelAdmin):
    list_display = ['profile', 'address_name']
