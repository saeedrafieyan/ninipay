from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Profile, ShippingAddress, Children


class DetailUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email', 'url']


class ListProfileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Profile
        fields = ['email', 'gender', 'url']


class DetailProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ['email', 'first_name', 'last_name', 'gender', 'mobile_number', 'home_number']


class ListChildrenSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Children
        fields = ['child_name', 'gender', 'url']


class DetailChildrenSerializer(serializers.ModelSerializer):
    profile = Children.objects.all()

    class Meta:
        model = Children
        fields = ['profile', 'child_name', 'age', 'gender']


class ListShippingAddressSerializer(serializers.HyperlinkedModelSerializer):
    profile = ShippingAddress.objects.all()

    class Meta:
        model = ShippingAddress
        fields = ['profile', 'address_name', 'url']


class DetailShippingAddressSerializer(serializers.HyperlinkedModelSerializer):
    profile = ListShippingAddressSerializer

    class Meta:
        model = ShippingAddress
        fields = '__all__'



