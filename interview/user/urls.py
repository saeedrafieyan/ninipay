from django.urls import path, include

from rest_framework import routers

from user import views

router = routers.DefaultRouter()
router.register('user', views.UserViewSet)
router.register('profile', views.ProfileViewSet)
router.register('children', views.ChildrenViewSet)
router.register('shipping', views.shippingViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
