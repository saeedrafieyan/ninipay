# Generated by Django 3.1.1 on 2020-09-19 03:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0006_auto_20200915_1748'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='join_date',
            field=models.DateField(auto_now_add=True, null=True, verbose_name='زمان عضویت'),
        ),
    ]
