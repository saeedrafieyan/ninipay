from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    """
    in this class user can create a profile.
    """

    gender_option = [
        ('Male', 'مرد'),
        ('Female', 'زن')
    ]
    ways_option = [
        ('instagram', 'ایستاگرام'),
        ('twitter', 'توئیتر'),
        ('billboard', 'تبلیغات شهری'),
        ('divar', 'سایت دیوار'),
        ('other', 'راه های دیگر'),
    ]
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.EmailField('ایمیل', max_length=254, blank=True)
    first_name = models.CharField('نام', max_length=50, blank=True)
    last_name = models.CharField('نام خانوادگی', max_length=50, blank=True)
    gender = models.CharField('جنسیت', max_length=10, choices=gender_option, blank=True)
    get_familiar = models.CharField('راه آشنایی شما', max_length=15, choices=ways_option, blank=True)
    mobile_number = models.CharField('موبایل', max_length=11, blank=True)
    home_number = models.CharField('تلفن', max_length=11, blank=True)
    join_date = models.DateField('زمان عضویت', auto_now_add=True, null=True)


    def __str__(self):
        return self.user.username


class Children(models.Model):
    """
    In this class, the user can enter the details of his/her children.
    """
    gender_option = [
        ('boy', 'پسر'),
        ('girl', 'دختر')
    ]
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    child_name = models.CharField('نام', max_length=50)
    age = models.FloatField('سن', )
    gender = models.CharField('جنسیت', max_length=10, choices=gender_option)

    def __str__(self):
        return self.profile.user.username


class ShippingAddress(models.Model):
    """
    in this class user can enter the shipping address
    """
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    address_name = models.CharField('نام آدرس', max_length=50)
    state = models.CharField('استان', max_length=30, null=True)
    city = models.CharField('شهر', max_length=30, null=True)
    address = models.CharField('آدرس', max_length=50, null=True)
    pelak = models.CharField('پلاک', max_length=5, blank=True)
    postal_code = models.CharField('کد پستی', max_length=11, blank=True)

    def __str__(self):
        return self.profile.user.username
